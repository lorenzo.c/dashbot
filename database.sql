create table area
(
	`_id` int auto_increment
		primary key,
	description varchar(200) null
)
;

create table cluster
(
	`_id` int auto_increment
		primary key,
	description varchar(200) null,
	area_id int not null,
	constraint FKfowupqulcggk6b4bjj6kxjnmu
	foreign key (area_id) references area (_id)
)
;

create index FKfowupqulcggk6b4bjj6kxjnmu
	on cluster (area_id)
;

create table inefficiency_rate
(
	`_id` int auto_increment
		primary key,
	robot_id int null,
	down_time int null,
	total_time int null,
	timestamp timestamp null,
	ir_value double null
)
;

create index inefficiency_rate_robot__id_fk
	on inefficiency_rate (robot_id)
;

create table inefficiency_rate_cluster
(
	`_id` int auto_increment
		primary key,
	cluster_id int null,
	down_time int null,
	total_time int null,
	robot_num int null,
	ir_value double null,
	constraint inefficiency_rate_cluster__id_fk
	foreign key (cluster_id) references cluster (_id)
)
;

create index inefficiency_rate_cluster__id_fk
	on inefficiency_rate_cluster (cluster_id)
;

create table robot
(
	`_id` int auto_increment
		primary key,
	cluster_id int null,
	constraint robot_cluster__id_fk
	foreign key (cluster_id) references cluster (_id)
)
;

create index robot_cluster__id_fk
	on robot (cluster_id)
;

alter table inefficiency_rate
	add constraint inefficiency_rate_robot__id_fk
foreign key (robot_id) references robot (_id)
;

create table robot_signal
(
	`_id` int auto_increment
		primary key,
	laser int not null,
	plate int not null,
	room_temperature int not null,
	room_contamination int not null,
	room_pressure int not null,
	injector_pressure int not null,
	injector_temperature int not null,
	robot_id int not null,
	timestamp timestamp default CURRENT_TIMESTAMP not null,
	constraint FKuy77ol27ejcb7jc219uymu0s
	foreign key (robot_id) references robot (_id)
)
;

create index FKuy77ol27ejcb7jc219uymu0s
	on robot_signal (robot_id)
;

create procedure calculateInefficiencyRate ()
	BEGIN
		DECLARE signal_id INT;
		DECLARE laser INT;
		DECLARE plate INT;
		DECLARE room_temperature INT;
		DECLARE room_contamination INT;
		DECLARE room_pressure INT;
		DECLARE injector_pressure INT;
		DECLARE injector_temperature INT;
		DECLARE robot_id INT;
		DECLARE signal_timestamp TIMESTAMP;

		DECLARE signal_id_1 INT;
		DECLARE laser_1 INT;
		DECLARE plate_1 INT;
		DECLARE room_temperature_1 INT;
		DECLARE room_contamination_1 INT;
		DECLARE room_pressure_1 INT;
		DECLARE injector_pressure_1 INT;
		DECLARE injector_temperature_1 INT;
		DECLARE robot_id_1 INT;
		DECLARE signal_timestamp_1 TIMESTAMP;

		DECLARE total_time INT DEFAULT 0;
		DECLARE down_time INT DEFAULT 0;

		DECLARE end_select_results INT DEFAULT 0;

		DECLARE now TIMESTAMP;

		DECLARE curs_robot CURSOR FOR
			SELECT
				a.`_id`,
				laser,
				plate,
				room_temperature,
				room_contamination,
				room_pressure,
				injector_pressure,
				injector_temperature,
				b.`_id`,
				timestamp
			FROM robot b
				LEFT JOIN robot_signal a ON a.robot_id = b.`_id` AND TIMESTAMPDIFF(MINUTE, a.timestamp, CURRENT_TIMESTAMP) <= 60
																		AND TIMESTAMPDIFF(MINUTE, a.timestamp, CURRENT_TIMESTAMP) >= 0
			ORDER BY a.robot_id, a.timestamp ASC;

		DECLARE CONTINUE HANDLER FOR NOT FOUND SET end_select_results = 1;

		OPEN curs_robot;

		FETCH curs_robot
		INTO signal_id, laser, plate, room_temperature, room_contamination, room_pressure,
			injector_pressure, injector_temperature, robot_id, signal_timestamp;

		FETCH curs_robot
		INTO signal_id_1, laser_1, plate_1, room_temperature_1, room_contamination_1, room_pressure_1,
			injector_pressure_1, injector_temperature_1, robot_id_1, signal_timestamp_1;

		SET now = current_timestamp();

		TRUNCATE inefficiency_rate;

		REPEAT

			IF (robot_id != robot_id_1) THEN
				IF (total_time = 0) THEN
					SELECT TIMESTAMPDIFF(SECOND, a.timestamp, now) AS total_time,
								 IF(a.laser=0 OR a.plate= 0 OR a.room_temperature=0 OR a.room_contamination=0 OR a.room_pressure=0 OR
										a.injector_pressure=0 OR a.injector_temperature=0, TIMESTAMPDIFF(SECOND, a.timestamp, now), 0) AS down_time
					INTO total_time, down_time
					FROM robot_signal a
					WHERE a.`_id` = robot_id
					ORDER BY timestamp DESC
					LIMIT 1;
				END IF;
				INSERT INTO inefficiency_rate (robot_id, total_time, down_time, timestamp) VALUES (robot_id, total_time, down_time, now);
				SET total_time = 0;
				SET down_time = 0;

			END IF;

			IF (robot_id = robot_id_1) THEN
				SET total_time = total_time + TIMESTAMPDIFF(SECOND,signal_timestamp,signal_timestamp_1);

				IF (laser=0 OR plate= 0 OR room_temperature=0 OR room_contamination=0 OR room_pressure=0 OR
						injector_pressure=0 OR injector_temperature=0) THEN
					SET down_time = down_time + TIMESTAMPDIFF(SECOND,signal_timestamp,signal_timestamp_1);
				END IF;
			END IF;

			SET signal_id = signal_id_1;
			SET laser = laser_1;
			SET plate = plate_1;
			SET room_temperature = room_temperature_1;
			SET room_contamination = room_contamination_1;
			SET room_pressure = room_pressure_1;
			SET injector_pressure = injector_pressure_1;
			SET injector_temperature = injector_temperature_1;
			SET robot_id = robot_id_1;
			SET signal_timestamp = signal_timestamp_1;


			FETCH curs_robot
			INTO signal_id_1, laser_1, plate_1, room_temperature_1, room_contamination_1, room_pressure_1,
				injector_pressure_1, injector_temperature_1, robot_id_1, signal_timestamp_1;

		UNTIL end_select_results END REPEAT;
		CLOSE curs_robot;

		IF (total_time = 0) THEN
			SELECT TIMESTAMPDIFF(SECOND, a.timestamp, now) AS total_time,
						 IF(a.laser=0 OR a.plate= 0 OR a.room_temperature=0 OR a.room_contamination=0 OR a.room_pressure=0 OR
								a.injector_pressure=0 OR a.injector_temperature=0, TIMESTAMPDIFF(SECOND, a.timestamp, now), 0) AS down_time
			INTO total_time, down_time
			FROM robot_signal a
			WHERE a.`_id` = robot_id
			ORDER BY timestamp DESC
			LIMIT 1;
		END IF;

		INSERT INTO inefficiency_rate (robot_id, total_time, down_time, timestamp) VALUES (robot_id_1, total_time, down_time, now);

	END;

create procedure calculateInefficiencyRateCluster ()
	BEGIN

		TRUNCATE inefficiency_rate_cluster;

		INSERT INTO inefficiency_rate_cluster (total_time, cluster_id, robot_num)
			SELECT
				TIMESTAMPDIFF(SECOND, MIN(c.timestamp), CURRENT_TIMESTAMP()) AS total_time,
				a.`_id`,
				count(b.`_id`)                                               AS robot_num
			FROM cluster a
				LEFT JOIN robot b ON a.`_id` = b.cluster_id
				LEFT JOIN robot_signal c ON b.`_id` = c.robot_id
			WHERE TIMESTAMPDIFF(MINUTE, c.timestamp, CURRENT_TIMESTAMP) <= 60
						AND TIMESTAMPDIFF(MINUTE, c.timestamp, CURRENT_TIMESTAMP) >= 0
			GROUP BY a.`_id`;

		DROP TABLE IF EXISTS CLUSTER_IR;

		CREATE TABLE CLUSTER_IR (
			_id        INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
			cluster_id INT,
			down       INT,
			timestamp  TIMESTAMP
		);

		INSERT INTO CLUSTER_IR (cluster_id, down, timestamp)
			SELECT
				a._id                                                                 AS cluster_id,
				IF(MIN(laser) = 0 OR MIN(plate) = 0 OR MIN(room_temperature) = 0 OR MIN(room_contamination) = 0 OR
					 MIN(room_pressure) = 0 OR
					 MIN(injector_pressure) = 0 OR MIN(injector_temperature) = 0, 0, 1) AS down,
				c.timestamp                                                           AS timestamp
			FROM cluster a
				LEFT JOIN robot b ON a.`_id` = b.cluster_id
				LEFT JOIN robot_signal c ON b.`_id` = c.robot_id
			WHERE TIMESTAMPDIFF(MINUTE, c.timestamp, CURRENT_TIMESTAMP) <= 60
						AND TIMESTAMPDIFF(MINUTE, c.timestamp, CURRENT_TIMESTAMP) >= 0
			GROUP BY a._id, c.timestamp, c.`_id`
			ORDER BY a.`_id`, c.`_id`;

		UPDATE inefficiency_rate_cluster f, (
																					SELECT
																						SUM(TIMESTAMPDIFF(SECOND, b.timestamp, a.timestamp)) AS down_time,
																						a.cluster_id
																					FROM CLUSTER_IR a
																						JOIN CLUSTER_IR b ON a.`_id` - 1 = b.`_id` AND a.cluster_id = b.cluster_id
																					WHERE b.down = 0
																					GROUP BY b.cluster_id) g
		SET f.down_time = g.down_time
		WHERE f.cluster_id = g.cluster_id;

		DROP TABLE IF EXISTS CLUSTER_IR;
	END;

