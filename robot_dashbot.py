# import requests

# headers = {
#     'accept': 'application/json',
#     'Content-Type': 'application/json',
# }

# data = '{"laser": 1, "robotId": 1}'

# counter = 0
# while counter <= 90000:
# 	response = requests.post('http://localhost:8080/signal', headers=headers, data=data)
# 	print("Request #" + str(counter) + "Response code: " + str(response.status_code))
# 	counter += 1


import asyncio
import json 
import random

from aiohttp import ClientSession

headers = {
'accept': 'application/json',
'Content-Type': 'application/json',
}

data = {}
robotids = [1, 2]

async def requests():
	counter = 0
	while counter <= 90000:
		async with ClientSession() as session:
			data['robotId'] = random.choice(robotids)
			data['laser'] = 1
			data['plate'] = 1
			data['room_temperature'] = 1
			data['room_pressure'] = 1
			data['room_contamination'] = 1
			data['injector_pressure'] = 1
			if (random.random() > 0.5):
				data['injector_temperature'] = 1
			else: 
				data['injector_temperature'] = 0
			async with session.request("POST", "http://localhost:8080/signal", data=json.dumps(data), headers=headers) as response:
				response = await response.read()
				print("Request n° "+ str(counter) + ", response: ")
				print(response)
				counter += 1

loop = asyncio.get_event_loop()

loop.run_until_complete(requests())
