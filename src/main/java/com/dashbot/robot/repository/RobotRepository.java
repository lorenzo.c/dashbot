package com.dashbot.robot.repository;

import com.dashbot.robot.entity.Robot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("robotRepository")
public interface RobotRepository extends JpaRepository<Robot, Long> {
    List<Robot> findByClusterId(int clusterId);
}
