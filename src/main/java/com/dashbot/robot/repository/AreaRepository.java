package com.dashbot.robot.repository;

import com.dashbot.robot.entity.Area;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("areaRepository")
public interface AreaRepository extends JpaRepository<Area, Long>, AreaViewRepository {
}