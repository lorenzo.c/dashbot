package com.dashbot.robot.repository;

import com.dashbot.robot.entity.Cluster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("clusterRepository")
public interface ClusterRepository extends JpaRepository<Cluster, Long>, ClusterViewRepository {
    List<Cluster> findByAreaId(int areaId);
}
