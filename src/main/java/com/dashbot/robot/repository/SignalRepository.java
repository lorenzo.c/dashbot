package com.dashbot.robot.repository;

import com.dashbot.robot.entity.Signal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("signalRepository")
public interface SignalRepository extends JpaRepository<Signal, Long> {
    List<Signal> findByRobotId(int idRobot);
}
