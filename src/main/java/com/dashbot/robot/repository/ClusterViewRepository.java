package com.dashbot.robot.repository;

import com.dashbot.robot.entity.AreaView;
import com.dashbot.robot.entity.ClusterView;

import java.util.List;

public interface ClusterViewRepository {
    List<ClusterView> getClustersAndIr(int areaId);
}
