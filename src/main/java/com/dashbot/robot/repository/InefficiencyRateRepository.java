package com.dashbot.robot.repository;

import com.dashbot.robot.entity.Area;
import com.dashbot.robot.entity.InefficiencyRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("inefficiencyRateRepository")
public interface InefficiencyRateRepository extends JpaRepository<InefficiencyRate, Long> {
    InefficiencyRate findInefficiencyRateByRobotId(int robotId);
}
