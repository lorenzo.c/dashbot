package com.dashbot.robot.repository;

import com.dashbot.robot.entity.AreaView;

import java.util.List;

public interface AreaViewRepository {
    List<AreaView> getAreaAndIr();
}
