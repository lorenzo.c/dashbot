package com.dashbot.robot.repository;

import com.dashbot.robot.entity.AreaView;
import com.dashbot.robot.entity.ClusterView;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class ClusterViewRepositoryImpl implements ClusterViewRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<ClusterView> getClustersAndIr(int areaId) {
        TypedQuery<ClusterView> query = em.createQuery(
    "SELECT new com.dashbot.robot.entity.ClusterView(b.id, b.areaId, d.robotNum, b.description, d.downTime, d.totalTime) FROM Cluster b " +
            "  LEFT JOIN InefficiencyRateCluster d ON b.id= d.clusterId " +
            "  WHERE b.areaId = :area_id ",
                ClusterView.class);
        query.setParameter("area_id", areaId);
        List<ClusterView> result = query.getResultList();
        result.forEach(a -> a.setIrValue(((float)a.getDownTime() / a.getTotalTime())));
        return result;
    }
}
