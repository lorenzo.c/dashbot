package com.dashbot.robot.repository;

import com.dashbot.robot.entity.AreaView;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class AreaViewRepositoryImpl implements AreaViewRepository {
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<AreaView> getAreaAndIr() {
        TypedQuery<AreaView> query = em.createQuery("SELECT new com.dashbot.robot.entity.AreaView(a.areaId, a.description, AVG(d.downTime/d.totalTime)) FROM Area a " +
                        "  LEFT JOIN Cluster b ON a.areaId = b.areaId " +
                        "  LEFT JOIN Robot c ON b.id = c.clusterId " +
                        "  LEFT JOIN InefficiencyRate d ON c.robotId = d.robotId " +
                        "GROUP BY a.areaId",
                AreaView.class);
        return query.getResultList();
    }
}
