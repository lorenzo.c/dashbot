package com.dashbot.robot.model;

import com.dashbot.robot.entity.Cluster;
import com.dashbot.robot.entity.ClusterView;

import java.util.List;

public interface ClusterModel {
    List<Cluster> getClustersById(int areaId);

    List<ClusterView> getClustersAndIrById(int areaId);
}
