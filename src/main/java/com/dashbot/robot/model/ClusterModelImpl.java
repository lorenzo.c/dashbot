package com.dashbot.robot.model;

import com.dashbot.robot.entity.Cluster;
import com.dashbot.robot.entity.ClusterView;
import com.dashbot.robot.repository.ClusterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("clusterModel")
public class ClusterModelImpl implements ClusterModel {
    @Autowired
    private ClusterRepository clusterRepository;

    @Override
    public List<Cluster> getClustersById(int areaId) {
        return clusterRepository.findByAreaId(areaId);
    }

    @Override
    public List<ClusterView> getClustersAndIrById(int areaId) {
        return clusterRepository.getClustersAndIr(areaId);
    }
}
