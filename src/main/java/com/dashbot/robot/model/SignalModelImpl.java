package com.dashbot.robot.model;

import com.dashbot.robot.entity.InefficiencyRate;
import com.dashbot.robot.entity.Signal;
import com.dashbot.robot.repository.InefficiencyRateRepository;
import com.dashbot.robot.repository.SignalRepository;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Service("signalModel")
public class SignalModelImpl implements SignalModel {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private SignalRepository signalRepository;

    @Autowired
    private InefficiencyRateRepository inefficiencyRateRepository;

    @Override
    public InefficiencyRate getInefficiencyRate(int idRobot) {
        InefficiencyRate inefficiencyRate = inefficiencyRateRepository.findInefficiencyRateByRobotId(idRobot);
        if (inefficiencyRate != null)
            inefficiencyRate.setIrValue(((double)inefficiencyRate.getDownTime() / (double)inefficiencyRate.getTotalTime()) * 100.0);
        return inefficiencyRate;
    }

    @Override
    public void launchIRCalculation() {
        StoredProcedureQuery query = entityManager
                .createStoredProcedureQuery("calculateInefficiencyRate");
        query.execute();
    }

    @Override
    public void launchClusterIRCalculation() {
        StoredProcedureQuery query = entityManager
                .createStoredProcedureQuery("calculateInefficiencyRateCluster");
        query.execute();
    }

    @Override
    @Async
    public void saveSignal(Signal signal) {
         signalRepository.save(signal);
    }

    @Override
    public List<Signal> getSignals(int idRobot) {
        return signalRepository.findByRobotId(idRobot);
    }
}
