package com.dashbot.robot.model;

import com.dashbot.robot.entity.Robot;
import com.dashbot.robot.repository.RobotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("robotModel")
public class RobotModelImpl implements RobotModel {
    @Autowired
    private RobotRepository robotRepository;

    @Override
    public List<Robot> getRobots(int clusterId) {
        return robotRepository.findByClusterId(clusterId);
    }
}
