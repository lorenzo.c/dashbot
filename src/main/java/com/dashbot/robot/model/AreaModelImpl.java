package com.dashbot.robot.model;

import com.dashbot.robot.entity.Area;
import com.dashbot.robot.entity.AreaView;
import com.dashbot.robot.entity.Cluster;
import com.dashbot.robot.repository.AreaRepository;
import com.dashbot.robot.repository.ClusterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("areaModel")
public class AreaModelImpl implements AreaModel {
    @Autowired
    private AreaRepository areaRepository;

    @Override
    public List<Area> getAreas() {
        return areaRepository.findAll();
    }

    @Override
    public List<AreaView> getAreaAndIr() {
        return areaRepository.getAreaAndIr();
    }
}
