package com.dashbot.robot.model;

import com.dashbot.robot.entity.Area;
import com.dashbot.robot.entity.Robot;
import com.dashbot.robot.entity.Signal;

import java.util.List;

public interface RobotModel {
    List<Robot> getRobots(int clusterId);
}
