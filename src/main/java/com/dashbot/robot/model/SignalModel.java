package com.dashbot.robot.model;

import com.dashbot.robot.entity.InefficiencyRate;
import com.dashbot.robot.entity.Signal;
import java.util.List;

public interface SignalModel {
    void saveSignal(Signal signal);

    List<Signal> getSignals(int idRobot);

    void launchIRCalculation();

    InefficiencyRate getInefficiencyRate(int idRobot);

    void launchClusterIRCalculation();
}
