package com.dashbot.robot.model;

import com.dashbot.robot.entity.Area;
import com.dashbot.robot.entity.AreaView;
import com.dashbot.robot.entity.Cluster;

import java.util.List;

public interface AreaModel {

    List<Area> getAreas();

    List<AreaView> getAreaAndIr();
}
