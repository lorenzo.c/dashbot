package com.dashbot.robot;

import com.dashbot.robot.model.SignalModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
    @Autowired
    private SignalModel signalModel;

    @Scheduled(cron = "0 0/10 * * * *")
    public void launchIRCalculation() {
        signalModel.launchIRCalculation();
    }

    @Scheduled(cron = "0 0/10 * * * *")
    public void launchClusterIRCalculation() {
        signalModel.launchClusterIRCalculation();
    }
}
