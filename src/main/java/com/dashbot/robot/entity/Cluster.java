package com.dashbot.robot.entity;

import javax.persistence.*;

@Entity
@Table(name = "cluster")
public class Cluster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id")
    private Integer id;
    @OneToOne
    @JoinColumn(name = "area_id", insertable = false, updatable = false)
    private Area area;
    @Column(name = "area_id")
    private Integer areaId;
    @Column(name = "description")
    private String description;

    public void setClusterId(int clusterId) {
        this.id = clusterId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public int getId() {

        return id;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }
}
