package com.dashbot.robot.entity;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "robot_signal")
public class Signal {
    @Id
//    @GeneratedValue(strategy= GenerationType.SEQUENCE)
//    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "_id")
    private int id;
    @OneToOne
    @JoinColumn(name = "robot_id", insertable = false, updatable = false)
    private Robot robots;
    @Column(name = "robot_id")
    private int robotId;
    @Column(name = "laser")
    @NotNull(message = "*Please provide a laser")
    private int laser;
    @Column(name = "plate")
    @NotNull(message = "*Please provide a plate")
    private int plate;
    @Column(name = "room_temperature")
    @NotNull(message = "*Please provide a room temperature")
    private int room_temperature;
    @Column(name = "room_contamination")
    @NotNull(message = "*Please provide a room contamination")
    private int room_contamination;
    @Column(name = "room_pressure")
    @NotNull(message = "*Please provide a room pressure")
    private int room_pressure;
    @Column(name = "injector_pressure")
    @NotNull(message = "*Please provide a injector pressure")
    private int injector_pressure;
    @Column(name = "injector_temperature")
    @NotNull(message = "*Please provide an injector temperature")
    private int injector_temperature;
    @Column(name = "timestamp")
    private String timestamp;


    public int getLaser() { return laser; }

    public void setLaser(int laser) {
        this.laser = laser;
    }

    public int getPlate() {
        return plate;
    }

    public void setPlate(int plate) {
        this.plate = plate;
    }

    public int getRoom_temperature() {
        return room_temperature;
    }

    public void setRoom_temperature(int room_temperature) {
        this.room_temperature = room_temperature;
    }

    public int getRoom_contamination() {
        return room_contamination;
    }

    public void setRoom_contamination(int room_contamination) {
        this.room_contamination = room_contamination;
    }

    public int getRoom_pressure() {
        return room_pressure;
    }

    public void setRoom_pressure(int room_pressure) {
        this.room_pressure = room_pressure;
    }

    public int getInjector_pressure() {
        return injector_pressure;
    }

    public void setInjector_pressure(int injector_pressure) {
        this.injector_pressure = injector_pressure;
    }

    public int getInjector_temperature() {
        return injector_temperature;
    }

    public void setInjector_temperature(int injector_temperature) {
        this.injector_temperature = injector_temperature;
    }

    public String getTimestamp() { return timestamp; }

    public boolean setTimestamp() { return false; }

    public Robot getRobots() {
        return robots;
    }

    public void setRobots(Robot robots) {
        this.robots = robots;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRobotId() {
        return robotId;
    }

    public void setRobotId(int robotId) {
        this.robotId = robotId;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getClass().isInstance(obj)) {
            return this.id == ((Signal) obj).getId();
        } else {
            return false;
        }
    }
}
