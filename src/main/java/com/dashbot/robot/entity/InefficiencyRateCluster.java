package com.dashbot.robot.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "inefficiency_rate_cluster")
public class InefficiencyRateCluster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id")
    private int _id;

    @OneToOne
    @JoinColumn(name = "cluster_id", insertable = false, updatable = false)
    private Cluster cluster;
    @Column(name = "cluster_id")
    private int clusterId;

    @Column(name = "total_time")
    private Integer totalTime;

    @Column(name = "down_time")
    private Integer downTime;

    @Column(name = "robot_num")
    private Integer robotNum;

    private Double irValue;


    public int get_id() { return _id; }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getDownTime() {
        return downTime;
    }

    public void setDownTime(int downTime) {
        this.downTime = downTime;
    }

    public int getClusterId() {
        return clusterId;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    public Integer getRobotNum() {
        return robotNum;
    }

    public void setRobotNum(Integer robotNum) {
        this.robotNum = robotNum;
    }

    public Double getIrValue() {
        return irValue;
    }

    public void setIrValue(Double irValue) {
        this.irValue = irValue;
    }
}
