package com.dashbot.robot.entity;

public class AreaView {
    public AreaView(int areaId, String description, Double irValue) {
        this.areaId = areaId;
        this.description = description;
        this.irValue = irValue;
    }

    private int areaId;
    private String description;
    private Double irValue;

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public Double getIrValue() {
        return irValue;
    }

    public void setIrValue(Double irValue) {
        this.irValue = irValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
