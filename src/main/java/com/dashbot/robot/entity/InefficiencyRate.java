package com.dashbot.robot.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table(name = "inefficiency_rate")
public class InefficiencyRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id")
    private int _id;

    @OneToOne
    @JoinColumn(name = "robot_id", insertable = false, updatable = false)
    private Robot robot;
    @Column(name = "robot_id")
    private int robotId;

    @Column(name = "total_time")
    private Integer totalTime;

    @Column(name = "down_time")
    private Integer downTime;

    private Double irValue;

    @Basic
    private Timestamp timestamp;

    public int get_id() { return _id; }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Robot getRobot() {
        return robot;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public int getRobotId() {
        return robotId;
    }

    public void setRobotId(int robotId) {
        this.robotId = robotId;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getDownTime() {
        return downTime;
    }

    public void setDownTime(int downTime) {
        this.downTime = downTime;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Double getIrValue() {
        return irValue;
    }

    public void setIrValue(Double irValue) {
        this.irValue = irValue;
    }
}
