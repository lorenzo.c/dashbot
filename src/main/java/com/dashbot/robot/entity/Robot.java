package com.dashbot.robot.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "robot")
public class Robot {
    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "_id")
    private int robotId;
    @OneToOne
    @JoinColumn(name = "cluster_id", insertable = false, updatable = false)
    private Cluster cluster;
    @Column(name = "cluster_id")
    private int clusterId;

    public int getRobotId() {
        return robotId;
    }

    public void setRobotId(int robotId) {
        this.robotId = robotId;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    public int getClusterId() {
        return clusterId;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }
    @Override
    public int hashCode() {
        return robotId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getClass().isInstance(obj)) {
            return this.robotId == ((Signal) obj).getId();
        } else {
            return false;
        }
    }
}
