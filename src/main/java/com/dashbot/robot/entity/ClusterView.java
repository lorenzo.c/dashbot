package com.dashbot.robot.entity;

public class ClusterView {
    private int clusterId;
    private int areaId;
    private int robotCount;
    private String description;
    private Float irValue;
    private int downTime;
    private int totalTime;

    public ClusterView(){}

    public ClusterView(int clusterId, int areaId, int robotCount, String description, int downTime, int totalTime) {
        this.clusterId = clusterId;
        this.areaId = areaId;
        this.robotCount = robotCount;
        this.description = description;
        this.downTime = downTime;
        this.totalTime = totalTime;
    }

    public int getClusterId() {
        return clusterId;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public int getRobotCount() {
        return robotCount;
    }

    public void setRobotCount(int robotCount) {
        this.robotCount = robotCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getIrValue() {
        return irValue;
    }

    public void setIrValue(Float irValue) {
        this.irValue = irValue;
    }

    public int getDownTime() {
        return downTime;
    }

    public void setDownTime(int downTime) {
        this.downTime = downTime;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }
}
