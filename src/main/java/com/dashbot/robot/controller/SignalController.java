package com.dashbot.robot.controller;

import com.dashbot.robot.entity.Signal;
import com.dashbot.robot.model.SignalModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class SignalController {

    @Autowired
    private SignalModel signalModel;

    @RequestMapping(consumes = "application/json", name = "/signal", produces = "application/json", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public String getSignal(@RequestBody Signal signal) {
        signalModel.saveSignal(signal);
        return "OK";
    }
}