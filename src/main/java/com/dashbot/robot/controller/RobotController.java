package com.dashbot.robot.controller;

import com.dashbot.robot.entity.InefficiencyRate;
import com.dashbot.robot.entity.Signal;
import com.dashbot.robot.model.SignalModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class RobotController {
    @Autowired
    private SignalModel signalModel;


    @GetMapping(value = "/dashboard/area/cluster/robot")
    public ModelAndView robots(@RequestParam int idRobot) {
        ModelAndView mav = new ModelAndView("robot");
        List<Signal> signals = signalModel.getSignals(idRobot);
        InefficiencyRate robotIR = signalModel.getInefficiencyRate(idRobot);
        mav.addObject("signals", signals);
        mav.addObject("robotIR", robotIR);
        return mav;
    }
}