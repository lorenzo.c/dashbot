package com.dashbot.robot.controller;

import com.dashbot.robot.entity.Cluster;
import com.dashbot.robot.entity.ClusterView;
import com.dashbot.robot.model.ClusterModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

@Controller
public class AreaController {
    @Autowired
    private ClusterModel clusterModel;

    @RequestMapping("dashboard/area")
    public ModelAndView areas(@RequestParam int idArea) {
        List<ClusterView> clusters = clusterModel.getClustersAndIrById(idArea);
        ModelAndView mav = new ModelAndView("area");
        mav.addObject("clusterList", clusters);
        return mav;
    }
}