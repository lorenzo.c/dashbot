package com.dashbot.robot.controller;

import com.dashbot.robot.model.RobotModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.dashbot.robot.entity.Robot;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ClusterController {
    @Autowired
    private RobotModel robotModel;

    @GetMapping("dashboard/area/cluster")
    public ModelAndView clusters(@RequestParam int idCluster) {
        List<Robot> robots = robotModel.getRobots(idCluster);
        ModelAndView mav = new ModelAndView("cluster");
        mav.addObject("robotList", robots);
        return mav;
    }
}
