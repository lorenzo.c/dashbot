package com.dashbot.robot.controller;

import com.dashbot.robot.entity.Area;
import com.dashbot.robot.entity.AreaView;
import com.dashbot.robot.model.AreaModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class DashboardController {
    @Autowired
    private AreaModel areaModel;

    @RequestMapping("/")
    public ModelAndView dashboard() {
        List<AreaView> areas = areaModel.getAreaAndIr();
        ModelAndView mav = new ModelAndView("dashboard");
        mav.addObject("areaList", areas);
        return mav;
    }
}
