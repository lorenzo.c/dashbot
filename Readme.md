DASHBOT 

A wonderful Spring application.


REQUIREMENTS:
- MySQL
- JDK 1.8
- Gradle


In order to run this project execute the following steps:

1) Git clone the project. 
2) Create a MySQL database and a user according to the credentials in application.properties.
3) Populate the database using the file database.sql: it contains all the tables and stored procedures you need.
4) Create some test data: you need at least 1 Area, 1 Cluster, 1 Robot.
5) Cd into the project and run gradle bootRun. 

At this point you can:
a) Run the python script "python3 robot_dashbot.py" to let the script send random signal to the application. 
This is useful especially if you want some real data to play with. This is script is the first version of the load test.
b) Having Apache Jmeter downloaded in your computer run "./jmeter -n -t dashbot_load_test.jmx -l robot_application_result.jtl" in order to run the load test.